package com.stockx.stockxdemo;

import com.stockx.stockxdemo.api.RedditModule;
import com.stockx.stockxdemo.view.PostsFragment;

import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(modules = RedditModule.class)
public interface AppComponent {
    void inject(PostsFragment frag);
}