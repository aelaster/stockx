package com.stockx.stockxdemo.api;

import com.stockx.stockxdemo.model.RedditResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RedditAPIInteractorImpl implements RedditAPIInteractor {
    private RedditAPI mService;

    RedditAPIInteractorImpl() {
        // Configure Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.reddit.com/r/")
                // Takes care of converting the JSON response into java objects
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mService = retrofit.create(RedditAPI.class);
    }

    @Override
    public Call<RedditResponse> getSubreddit(String subreddit, Integer record_count, String after_reddit_id) {
        return mService.getSubreddit(subreddit, record_count, after_reddit_id);
    }
}
