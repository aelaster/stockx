package com.stockx.stockxdemo.api;

import com.stockx.stockxdemo.model.RedditResponse;

import retrofit2.Call;

public interface RedditAPIInteractor {
    Call<RedditResponse> getSubreddit(String subreddit, Integer record_count, String after_reddit_id);
}
