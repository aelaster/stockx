package com.stockx.stockxdemo.api;

import com.stockx.stockxdemo.model.RedditResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RedditAPI {
    @GET("{subreddit}/.json")
    Call<RedditResponse> getSubreddit(@Path("subreddit") String subreddit, @Query("limit") Integer record_count, @Query("after") String after_reddit_id);
}