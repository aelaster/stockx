package com.stockx.stockxdemo.api;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class RedditModule {
    @Provides
    public RedditAPIInteractor providesWeatherAPIInteractor() {
        return new RedditAPIInteractorImpl();
    }
}