package com.stockx.stockxdemo.presenter;

import com.stockx.stockxdemo.api.RedditAPIInteractor;
import com.stockx.stockxdemo.model.RedditResponse;
import com.stockx.stockxdemo.view.PostsFragmentView;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RedditPresenter {
    private PostsFragmentView mCurrentView;
    private RedditAPIInteractor mInteractor;

    @Inject
    RedditPresenter(RedditAPIInteractor interactor) {
        this.mInteractor = interactor;
    }

    public void bind(PostsFragmentView view) { this.mCurrentView = view; }
    public void unbind() {
        mCurrentView = null;
    }

    public void getSubreddit(final String subreddit, Integer record_count, String after_reddit_id) {
        mInteractor.getSubreddit(subreddit, record_count, after_reddit_id)
            // enqueue runs the request on a separate thread
            .enqueue(new Callback<RedditResponse>() {
                // We receive a Response with the content we expect already parsed
                @Override
                public void onResponse(@NotNull Call<RedditResponse> call,
                                       @NotNull Response<RedditResponse> response) {
                    if (mCurrentView != null)
                        mCurrentView.updateUi(response.body(), subreddit);
                }

                // In case of error, this method gets called
                @Override
                public void onFailure(@NotNull Call<RedditResponse> call, @NotNull Throwable t) {
                    t.printStackTrace();
                    if (mCurrentView != null)
                        mCurrentView.updateUi(null, null);
                }
            });
    }
}
