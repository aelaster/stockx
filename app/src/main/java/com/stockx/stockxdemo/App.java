package com.stockx.stockxdemo;

import android.support.multidex.MultiDexApplication;

public class App extends MultiDexApplication {
    public static final int POSTS_AT_A_TIME = 25;
    public static final String DEFAULT_SUBREDDIT = "all";
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.create();
    }
    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}