package com.stockx.stockxdemo.adapter;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.stockx.stockxdemo.MainActivity;
import com.stockx.stockxdemo.R;
import com.stockx.stockxdemo.model.Post;
import com.stockx.stockxdemo.view.PostFragment;

class ViewPostHandler implements View.OnClickListener {

    private Context mContext;
    private Post mCurrentPost;
    ViewPostHandler( Context context, Post currentPost ) {
        this.mContext = context;
        this.mCurrentPost = currentPost;
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = ((MainActivity)mContext).getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frame_layout, PostFragment.newInstance(mCurrentPost.getUrl()));
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
