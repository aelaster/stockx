package com.stockx.stockxdemo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stockx.stockxdemo.App;
import com.stockx.stockxdemo.R;
import com.stockx.stockxdemo.model.Post;
import com.stockx.stockxdemo.model.Posts;

import java.util.List;

public class RedditAdapter extends RecyclerView.Adapter<RedditAdapter.ViewHolder> {

    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_ACTIVITY = 1;

    private Context mContext;
    private List<Posts> mPosts;

    public RedditAdapter(List<Posts> posts, Context context) {
        mPosts = posts;
        mContext = context;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_LOADING) {
            // display the last row
            View view = getFooterView(parent);
            return new ViewHolder(view);
        }

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_posts_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        if (position < mPosts.size()) {
            Post currentPost = mPosts.get(position).getPost();

            holder.mPostTitle.setText(currentPost.getTitle());
            holder.mPostSubreddit.setText(currentPost.getSubreddit());

            Picasso.with(mContext).load(currentPost.getThumbnail()).into(holder.mPostThumb);

            holder.mClickLayout.setOnClickListener(new ViewPostHandler(mContext, currentPost));
        }
    }

    @Override
    public int getItemCount() {
        if (mPosts.size() >= App.POSTS_AT_A_TIME) {
            return mPosts.size() + 1; //+ 1 is for the progress bar
        }else{
            return mPosts.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position >= mPosts.size() && mPosts.size() >= App.POSTS_AT_A_TIME) ? VIEW_TYPE_LOADING
                : VIEW_TYPE_ACTIVITY;
    }

    @Override
    public long getItemId(int position) {
        return (getItemViewType(position) == VIEW_TYPE_ACTIVITY) ? position
                : -1;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView mPostTitle,mPostSubreddit;
        private final ImageView mPostThumb;
        private final ConstraintLayout mClickLayout;

        private ViewHolder(View view) {
            super(view);
            mPostTitle = view.findViewById(R.id.post_title);
            mPostSubreddit = view.findViewById(R.id.post_subreddit);
            mPostThumb = view.findViewById(R.id.post_thumb);
            mClickLayout = view.findViewById(R.id.click_layout);
        }
    }

    private View getFooterView(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_bar, parent, false);
    }


}