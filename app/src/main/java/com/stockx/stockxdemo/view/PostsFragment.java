package com.stockx.stockxdemo.view;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.stockx.stockxdemo.App;
import com.stockx.stockxdemo.R;
import com.stockx.stockxdemo.adapter.RedditAdapter;
import com.stockx.stockxdemo.model.Posts;
import com.stockx.stockxdemo.model.RedditResponse;
import com.stockx.stockxdemo.presenter.RedditPresenter;

import java.util.List;

import javax.inject.Inject;

public class PostsFragment extends Fragment implements PostsFragmentView {

    private static final String SUBREDDIT = "subreddit";
    private static final String RECORD_COUNT = "record_count";
    private static final String AFTER_REDDIT_ID = "after_reddit_id";

    private RedditPresenter mPresenter;
    private RedditAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgress;
    private EndlessRecyclerViewScrollListener mScrollListener;

    private AlertDialog mDialog;

    private String mSubreddit = App.DEFAULT_SUBREDDIT;
    private Integer mRecordCount = App.POSTS_AT_A_TIME;
    private String mAfterRedditId = null;
    private List<Posts> mPosts = null;

    public PostsFragment() {
    }

    public static PostsFragment newInstance(String subreddit, Integer record_count, String after_reddit_id) {
        PostsFragment fragment = new PostsFragment();
        Bundle args = new Bundle();
        args.putString(SUBREDDIT, subreddit);
        args.putInt(RECORD_COUNT, record_count);
        args.putString(AFTER_REDDIT_ID, after_reddit_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mSubreddit = getArguments().getString(SUBREDDIT);
            mRecordCount = getArguments().getInt(RECORD_COUNT);
            mAfterRedditId = getArguments().getString(AFTER_REDDIT_ID);
        }
    }

    @Inject
    public void setPresenter(RedditPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onAttach(Context context) {
        ((App)(context.getApplicationContext())).getAppComponent().inject(this);
        mPresenter.bind(this);
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        mPresenter.unbind();
        super.onDetach();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_posts, container, false);

        // Set the adapter
        Context context = rootView.getContext();
        mRecyclerView = rootView.findViewById(R.id.list);
        mProgress = rootView.findViewById(R.id.pbProgress);
        LinearLayoutManager layoutManager =new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(layoutManager);
        mScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                if (totalItemsCount >= App.POSTS_AT_A_TIME) {
                    mPresenter.getSubreddit(mSubreddit, mRecordCount, mAfterRedditId);
                }
            }
        };
        mRecyclerView.addOnScrollListener(mScrollListener);

        FloatingActionButton fab = rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSubredditDialog(view);
            }
        });

        mPresenter.getSubreddit(mSubreddit, mRecordCount, mAfterRedditId);
        return rootView;
    }

    @Override
    public void updateUi(RedditResponse current, String subreddit) {
        mProgress.setVisibility(View.GONE);
        if (current != null){
            if(current.getData().getPosts().size() > 0){
                if (mPosts == null || !subreddit.equals(mSubreddit)) {
                    if (mScrollListener != null) {
                        mScrollListener.resetState();
                    }
                    mPosts = current.getData().getPosts();
                    mAdapter = new RedditAdapter(mPosts, requireContext());
                    mRecyclerView.setAdapter(mAdapter);
                } else {
                    mPosts.addAll(current.getData().getPosts());
                    mAdapter.notifyDataSetChanged();
                }

                mSubreddit = subreddit;
                mAfterRedditId = current.getData().getAfterId();
            }else {
                Snackbar mySnackbar = Snackbar.make(mRecyclerView, R.string.subreddit_invalid, Snackbar.LENGTH_LONG);
                mySnackbar.show();
            }
        }else{
            Snackbar mySnackbar = Snackbar.make(mRecyclerView, R.string.server_connection_error, Snackbar.LENGTH_LONG);
            mySnackbar.show();
        }
    }


    private void openSubredditDialog(final View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());

        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        View customView = inflater.inflate(R.layout.content_change_subreddit, null);
        builder.setView(customView);

        //handle the enter press on the soft keyboard
        final EditText userInput = customView.findViewById(R.id.subreddit);
        userInput.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    Button theButton = mDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    theButton.performClick();
                    return true;
                }
                return false;
            }
        });

        builder.setTitle(R.string.subreddit_title);
        builder.setPositiveButton(R.string.subreddit_submit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String the_subreddit = userInput.getText().toString();

                //trim off /r/
                if (the_subreddit.startsWith("/r/")) {
                    the_subreddit = the_subreddit.substring(3, the_subreddit.length());
                }

                if (the_subreddit.startsWith("_")) {
                    Snackbar mySnackbar = Snackbar.make(view, R.string.subreddit_invalid, Snackbar.LENGTH_LONG);
                    mySnackbar.show();
                }else {
                    if (the_subreddit.length() < 3) {
                        Snackbar mySnackbar = Snackbar.make(view, R.string.subreddit_short, Snackbar.LENGTH_LONG);
                        mySnackbar.show();
                    } else {
                        mProgress.setVisibility(View.VISIBLE);
                        mPresenter.getSubreddit(the_subreddit, mRecordCount, null);
                    }
                }
            }
        });

        mDialog = builder.create();

        //display keyboard on open
        if (mDialog.getWindow() != null) {
            mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }
        mDialog.show();
    }
}
