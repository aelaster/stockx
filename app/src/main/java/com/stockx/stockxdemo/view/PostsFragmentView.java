package com.stockx.stockxdemo.view;

import com.stockx.stockxdemo.model.RedditResponse;

public interface PostsFragmentView {
    void updateUi(RedditResponse current, String subreddit);
}