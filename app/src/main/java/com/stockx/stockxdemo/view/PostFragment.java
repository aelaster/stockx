package com.stockx.stockxdemo.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.stockx.stockxdemo.R;

public class PostFragment extends Fragment {

    private static final String URL = "url";

    private String mUrl;

    public PostFragment() {
    }

    public static PostFragment newInstance(String url) {
        PostFragment fragment = new PostFragment();
        Bundle args = new Bundle();
        args.putString(URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mUrl = getArguments().getString(URL);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_view, container, false);

        WebView webview = rootView.findViewById(R.id.web_view);

        //this keeps the web in the app
        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                view.loadUrl(url);
                return false; // then it is not handled by default action
            }
        });

        WebSettings websettings = webview.getSettings();
        websettings.setBuiltInZoomControls(true);
        websettings.setDisplayZoomControls(false);
        websettings.setJavaScriptEnabled(true);
        websettings.setSupportZoom(true);
        websettings.setLoadWithOverviewMode(true);
        websettings.setUseWideViewPort(true);
        websettings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        webview.loadUrl(mUrl);

        return rootView;
    }

}
