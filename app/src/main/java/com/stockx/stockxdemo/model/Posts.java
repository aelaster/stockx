package com.stockx.stockxdemo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Posts {
    @SerializedName("data")
    @Expose
    private Post post;

    public Post getPost() {
        return post;
    }
}
