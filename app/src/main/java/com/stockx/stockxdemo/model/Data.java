package com.stockx.stockxdemo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Data {
    @SerializedName("children")
    @Expose
    private ArrayList<Posts> posts;

    @SerializedName("after")
    @Expose
    private String afterId;

    public ArrayList<Posts> getPosts() {
        return posts;
    }

    public String getAfterId() {
        return afterId;
    }
}
